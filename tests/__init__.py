try:
    from trytond.modules.web_shop_woocommerce.tests.test_web_shop_woocommerce import suite  # noqa: E501
except ImportError:
    from .test_web_shop_woocommerce import suite

__all__ = ['suite']
